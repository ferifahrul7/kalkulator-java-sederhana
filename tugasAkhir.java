package tugasakhir;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 *
 * @author feeri
 */
public class TugasAkhir extends JFrame implements ActionListener {
 final JLabel label1, label2, label3, labelOperator, labelJudul;
 final JButton button;
 final JComboBox combo;
 final JTextField textField1, textField2, textField3;
 private float angka1;
 private float angka2;
 private float hasil;
 private char c;
 private String operator;

 public void setAngka1(float angka1) {
  this.angka1 = angka1;
 }
 public void setAngka2(float angka2) {
  this.angka2 = angka2;
 }
 public void setOperator(String operator) {
  this.operator = operator;
 }
 public float getHasil() {
  return hasil;
 }
 public void count() {
  switch (this.operator) {
   case "Tambah":
    this.hasil = this.angka1 + this.angka2;
    break;
   case "Kurang":
    this.hasil = this.angka1 - this.angka2;
    break;
   case "Kali":
    this.hasil = this.angka1 * this.angka2;
    break;
   case "Bagi":
    this.hasil = this.angka1 / this.angka2;
    break;
   default:
    this.hasil = 0;

  }

 }
 public boolean verify(String tf1,String tf2) {
            String text = tf1;
            String text2 = tf1;
            try {
                Float.parseFloat(text);
                Float.parseFloat(text2);
            }
            catch(NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Kamu Harus Input Angka!");
                return false;
            }
            return true;
        }

 public void actionPerformed(ActionEvent e) {
  if (e.getSource() == button) {
   //TugasAkhir hitung = new TugasAkhir();
   if(verify(textField1.getText(),textField2.getText()) == true){
   setAngka1(Float.parseFloat(textField1.getText()));
   setAngka2(Float.parseFloat(textField2.getText()));
   
  }
   setOperator((String) combo.getSelectedItem());
   if(combo.getSelectedItem()=="Pilih"){
       JOptionPane.showMessageDialog(null,"Silahkan pilih operator perhitungan");
   }else{
   count();
   textField3.setText(String.valueOf(getHasil()));
   }
  }
 }
 public TugasAkhir() {

  GridBagLayout grid = new GridBagLayout();
  GridBagConstraints gbc = new GridBagConstraints();
  setLayout(grid);

  //label kalkulator mini
  labelJudul = new JLabel("Kalkulator Mini");
  labelJudul.setFont(new Font("Verdana", Font.BOLD, 16));
  labelJudul.setBorder(BorderFactory.createEmptyBorder(0, 0, 15, 0));
  gbc.gridx = 0;
  gbc.gridy = 0;
  gbc.gridwidth = 4;
  this.add(labelJudul, gbc);

  //label nilai Pertama
  label1 = new JLabel("Nilai Pertama      ");
  gbc.gridx = 0;
  gbc.gridy = 2;
  gbc.gridwidth = 1;
  gbc.anchor = GridBagConstraints.WEST;
  this.add(label1, gbc);

  //text field nilai pertama
  textField1 = new JTextField(10);
  gbc.gridx = 1;
  gbc.gridy = 2;
  gbc.gridwidth = 1;
  this.add(textField1, gbc);
  

  //label Operator
  labelOperator = new JLabel("Operator        ");
  gbc.gridx = 0;
  gbc.gridy = 3;
  this.add(labelOperator, gbc);

  //combo box operator
  String pilihan[] = {
   "Pilih",
   "Tambah",
   "Kurang",
   "Kali",
   "Bagi"
  };
  combo = new JComboBox(pilihan);
  gbc.gridx = 1;
  gbc.gridy = 3;
  gbc.gridwidth = 1;
  this.add(combo, gbc);

  //label nilai kedua
  label2 = new JLabel("Nilai Kedua        ");
  gbc.gridx = 0;
  gbc.gridy = 4;
  gbc.gridwidth = 1;
  this.add(label2, gbc);

  //text field nilai kedua
  textField2 = new JTextField(10);
  gbc.gridx = 1;
  gbc.gridy = 4;
  gbc.gridwidth = 1;
  this.add(textField2, gbc);

  //label hasil
  label3 = new JLabel("Hasil      ");
  gbc.gridx = 0;
  gbc.gridy = 5;
  gbc.gridwidth = 1;
  this.add(label3, gbc);

  //text field hasil
  textField3 = new JTextField(10);
  gbc.gridx = 1;
  gbc.gridy = 5;
  gbc.gridwidth = 1;
  textField3.setEditable(false);
  this.add(textField3, gbc);

  //button hitung
  button = new JButton("Hitung");
  gbc.gridx = 0;
  gbc.gridy = 6;
  gbc.gridwidth = 1;
  gbc.gridwidth = 4;
  gbc.anchor = GridBagConstraints.CENTER;
  button.addActionListener(this);
  this.add(button, gbc);

  //pengaturan frame
  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  this.setSize(300, 300);
  this.setVisible(true);
 }


 public static void main(String args[]) {
  //menguji program dengan membuat objek dimetode main
  new TugasAkhir();
 }
}